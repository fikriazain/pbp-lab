from django.db import models

# Create your models here.

class Note(models.Model):
    to_user = models.CharField(max_length=100)
    from_user = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    message = models.TextField()