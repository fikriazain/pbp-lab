from django.shortcuts import render
from .forms import FriendForm
from django.http import HttpResponseRedirect, response
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    response = {"friends" : Friend.objects.all()}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    if request.method == 'POST':
        form = FriendForm(request.POST)
    
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-3')

    form = FriendForm()
    context = { 'form': form }
    return render(request, 'lab3_forms.html', context)

