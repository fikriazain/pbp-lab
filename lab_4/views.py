from django.shortcuts import render
from lab_2.models import Note
from django.http import HttpResponseRedirect
from .forms import NoteForm
from django.contrib.auth.decorators import login_required
# Create your views here.

def index(request):
    data = Note.objects.all()
    response = {'data':data}
    return render(request, "lab4_index.html", response)

@login_required(login_url='/admin/login/')
def add_note(request):
    if request.method == 'POST':
        form = NoteForm(request.POST)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-4')

    form = NoteForm()
    context = { 'form': form }
    return render(request, "lab4_form.html", context)

def note_list(request):
    response = {"data" :  Note.objects.all()}
    return render(request, "lab4_note_list.html", response)

@login_required(login_url='/admin/login/')
def delete_card(request, note):
    notes = Note.objects.get(id = note)
    notes.delete()
    return HttpResponseRedirect('/lab-4/note-list')


def delete_table(request, note):
    notes = Note.objects.get(id = note)
    notes.delete()
    return HttpResponseRedirect('/lab-4')