from django.urls import path
from .views import add_note, delete_table, index, note_list, delete_card, delete_table

urlpatterns = [
    path('', index, name='index'),
    path('add-note/', add_note),
    path('note-list/', note_list),
    path('delete-card/<int:note>', delete_card , name="delete_card"),
    path('delete-table/<int:note>', delete_table , name="delete_table")
    # TODO Add friends path using friend_list Views
]