# PERTANYAAN SINGKAT

#### 1. Apakah perbedaan antara JSON dan XML?
Perbedaan JSON dengan XML adalah
> - Pada JSON, filenya lebih ringan dibanding dengan XML
> - JSON memiliki struktur kode yang lebih sederhana dibanding XML
> - JSON Dapat mudah dipahami oleh manusia ketimbang XML
> - Struktur JSON lebih seperti dictionary sedangkan XML seperti HTML
> - XML lebih unggul dalam markup dokumen dan informasi metadata 
#### 2. Apakah perbedaan antara HTML dan XML?
Perbedaan HTML dengan XML adalah:
> - Fokus HTML yaitu menampilkan data pada user sedangkan XML lebih ke struktur dan konteks
> - XML memiliki sebuah framework untuk menentukan bahasa markup sedangkan HTML merupakan markup standar
> - XML case sensitive sedangkan HTML tidak